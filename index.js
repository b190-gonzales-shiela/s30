/*Activity:
1. Create an activity.js file on where to write and save the solution for the activity.
2. Use the count operator to count the total number of fruits on sale.
3. Use the count operator to count the total number of fruits with stock more than 20.
4. Use the average operator to get the average price of fruits onSale per supplier.
5. Use the max operator to get the highest price of a fruit per supplier.
6. Use the min operator to get the lowest price of a fruit per supplier.
7. Create a git repository named S30.
8. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
9. Add the link in Boodle.*/


/*2. Use the count operator to count the total number of fruits on sale.*/

db.fruits.aggregate([
	{$match: {onSale:true}},
	{$group:{_id:"$name",totalfruitsonsale:{$sum:"$stock"}}}
]);


/*3. Use the count operator to count the total number of fruits with stock more than 20.*/
db.fruits.aggregate([
	{$match: {stock:20}},
	{$group:{_id:"$name",totalstock:{$sum:"$stock"}}}
]);

/*Use the average operator to get the average price of fruits onSale per supplier.*/

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group:{_id:"$name",averageprice:{$avg:"$price"}}}
]);

/*Use the max operator to get the highest price of a fruit per supplier.*/
db.fruits.aggregate([
	{$match: {onSale: false}},
	{$group:{_id:"$name", maxprice:{$max:"$price"}}}
]);

/*Use the min operator to get the lowest price of a fruit per supplier.*/
db.fruits.aggregate([
	{$match: {price:20}},
	{$group:{_id:"$name",minprice:{$min:"$price"}}}
]);